package com.kim.usermanagment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    //Mockup คือ การจำลองข้อมูล
    static {     //บล็อกที่จะถูกโหลดเมื่อคลาสโหลด
        userList.add(new User("admin", "password")); //เมื่อเปิดโปรแกรมใหม่ admin จะอยู่ตลอดไป
        userList.add(new User("user1", "password")); //ข้อมูลที่ Mockup ขึ้น
    }

    // Create เกี่ยวข้องกับ function Add
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    // การทำแบบ Overload
    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    // Update นำ User มาอัพเดต
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    // Read 1 user 
    public static User getUser(int index) {
        if(index > userList.size() -1){
            return null;
        }
        return userList.get(index);
    }

    // Read all user 
    public static ArrayList<User> getUsers() {
        return userList;
    }

    // Search username
    public static ArrayList<User> searchUsername(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return userList;
    }

    //Delete user;
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    //Delete user;
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    // Login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public static void save(){
        

    }
    
    public static void load(){
    }
}
